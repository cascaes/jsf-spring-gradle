CREATE TABLE users (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(1000) NOT NULL,
    last_name VARCHAR(1000) NOT NULL
);

INSERT INTO users (first_name, last_name) VALUES('John', 'Collins'), ('Matthew', 'Kim');