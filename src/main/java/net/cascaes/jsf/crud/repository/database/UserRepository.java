package net.cascaes.jsf.crud.repository.database;


import net.cascaes.jsf.crud.repository.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
