package net.cascaes.jsf.crud.model;

import lombok.Getter;
import lombok.Setter;
import net.cascaes.jsf.crud.repository.database.UserRepository;
import net.cascaes.jsf.crud.repository.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.inject.Named;
import java.util.List;

@Named
@Component
@Getter
@Setter
public class BasicUser {

    @Autowired
    private UserRepository userRepository;

    private String firstName = "Caio";
    private String lastName = "Cascaes";

    public List<User> list() {
        return userRepository.findAll();
    }

    public void add() {
        User u = new User();
        u.setFirstName(firstName);
        u.setLastName(lastName);
        userRepository.save(u);
    }

    public String showGreeting() {
        return "Hello " + firstName + " " + lastName + "!";
    }
}
